import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UploadFormComponent } from './uploads/upload-form/upload-form.component';
import { UploadService } from './uploads/shared/upload.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDOWLR27A7sWqLbsTPm-wVRw7XZUsqOL_Y',
    authDomain: 'test-12254.firebaseapp.com',
    databaseURL: 'https://test-12254.firebaseio.com',
    projectId: 'test-12254',
    storageBucket: 'test-12254.appspot.com',
    messagingSenderId: '1061363293657'
    }
};

@NgModule({
  declarations: [
    AppComponent,
    UploadFormComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers: [AngularFireDatabase, UploadService],
  bootstrap: [AppComponent]
})
export class AppModule { }
